# TspSolver

A simple implementation of a [genetic algorithm][1] in Elixir using the [Travelling Salesman Problem][2] (TSP) as fodder.

[1](http://en.wikipedia.org/wiki/Genetic_algorithm)
[2](http://en.wikipedia.org/wiki/Travelling_salesman_problem)
