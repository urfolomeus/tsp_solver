defmodule TspSolver.Mixfile do
  use Mix.Project

  def project do
    [ app: :tsp_solver,
      version: "0.0.1",
      name: "TSP Solver",
      source_url: "https://bitbucket.org/urfolomeus/tsp_solver",
      elixir: "~> 0.12.4",
      env: [
        dev:  [deps: dev_deps],
        test: [deps: deps],
        prod: [deps: deps]
      ]
    ]
  end

  # Configuration for the OTP application
  def application do
    [mod: { TspSolver, [] }]
  end

  # Returns the list of dependencies in the format:
  # { :foobar, git: "https://github.com/elixir-lang/foobar.git", tag: "0.1" }
  #
  # To specify particular versions, regardless of the tag, do:
  # { :barbat, "~> 0.1", github: "elixir-lang/barbat" }
  defp deps do
    []
  end

  defp dev_deps do
    [
      { :ex_doc, github: "elixir-lang/ex_doc" }
    ] ++ deps
  end
end
