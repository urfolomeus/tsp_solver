defmodule FitnessTest do
  use ExUnit.Case

  test "calculates the fitness correctly" do
    individual = [:Aberdeen, :Dundee, :Perth, :Edinburgh, :Glasgow, :Inverness]
    assert TspSolver.Fitness.of_individual(individual) == 457
  end
end
