defmodule HelpersTest do
  use ExUnit.Case

  doctest TspSolver.Helpers

  test "provides 1 random element from the list" do
    :random.seed(1,2,3)
    actual_123 = TspSolver.Helpers.sample(population, 1)

    :random.seed(11,22,33)
    actual_112233 = TspSolver.Helpers.sample(population, 1)

    assert [ List.first(expected_123) ] == actual_123
    assert [ List.first(expected_112233) ] == actual_112233
  end

  test "provides 2 random elements from the list" do
    :random.seed(1,2,3)
    actual_123 = TspSolver.Helpers.sample(population, 2)

    :random.seed(11,22,33)
    actual_112233 = TspSolver.Helpers.sample(population, 2)

    assert expected_123 == actual_123
    assert expected_112233 == actual_112233
  end

  test "provides the index of the given element within the given colection" do
    solution_to_find = [:Inverness, :Perth, :Glasgow, :Edinburgh, :Dundee, :Aberdeen]
    assert TspSolver.Helpers.index_of(population, solution_to_find) == 2
  end

  test "returns nil if element not found" do
    solution_to_find = [:I, :Do, :Not, :Exist]
    assert TspSolver.Helpers.index_of(population, solution_to_find) == nil
  end

  defp population do
    [
      [:Aberdeen, :Dundee, :Edinburgh, :Glasgow, :Inverness, :Perth],
      [:Edinburgh, :Glasgow, :Perth, :Inverness, :Aberdeen, :Dundee],
      [:Inverness, :Perth, :Glasgow, :Edinburgh, :Dundee, :Aberdeen],
      [:Dundee, :Perth, :Edinburgh, :Aberdeen, :Glasgow, :Inverness],
      [:Perth, :Inverness, :Glasgow, :Aberdeen, :Edinburgh, :Dundee],
    ]
  end

  defp expected_123 do
    [
      [:Dundee, :Perth, :Edinburgh, :Aberdeen, :Glasgow, :Inverness],
      [:Edinburgh, :Glasgow, :Perth, :Inverness, :Aberdeen, :Dundee]
    ]
  end

  defp expected_112233 do
    [
      [:Inverness, :Perth, :Glasgow, :Edinburgh, :Dundee, :Aberdeen],
      [:Perth, :Inverness, :Glasgow, :Aberdeen, :Edinburgh, :Dundee]
    ]
  end
end
