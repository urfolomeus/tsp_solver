defmodule CrossoverTest do
  use ExUnit.Case

  test "it provides two new inviduals from the given parents using permutation crossover" do
    parents = [
      [:Edinburgh, :Glasgow, :Perth, :Inverness, :Aberdeen, :Dundee],
      [:Inverness, :Perth, :Glasgow, :Edinburgh, :Dundee, :Aberdeen],
    ]
    expected = [
      [:Edinburgh, :Glasgow, :Inverness, :Perth, :Dundee, :Aberdeen],
      [:Inverness, :Perth, :Edinburgh, :Glasgow, :Aberdeen, :Dundee],
    ]
    actual = TspSolver.Crossover.permutation(parents, 2)
    assert actual == expected
  end

  def contains?(list, sub_list) do
    Enum.all?( sub_list, &(Enum.member?(list, &1)) )
  end
end
