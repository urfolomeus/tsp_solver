defmodule GeneratorTest do
  use ExUnit.Case

  doctest TspSolver.Generator

  test "generates a population with n individuals" do
    assert Kernel.length(population) == 10
  end

  test "each individual is valid" do
    assert Enum.all?(population, &valid_solution?/1)
  end

  # Helpers

  def population, do: TspSolver.Generator.generate_population(10)

  def all_unique?(list) do
    unique_list = HashSet.new(list)
    Kernel.length(list) == Set.size(unique_list)
  end

  def same_length_as_city_list?(list) do
    Kernel.length(list) == Kernel.length(TspSolver.Generator.cities)
  end

  def valid_solution?(individual) do
    same_length_as_city_list?(individual) && all_unique?(individual)
  end
end
