defmodule MutatorTest do
  use ExUnit.Case

  test "it swaps the elements 1 and 3 when seed is {1,2,3}" do
    individual = [:Aberdeen, :Dundee, :Perth, :Edinburgh, :Glasgow, :Inverness]
    expected   = [:Aberdeen, :Edinburgh, :Perth, :Dundee, :Glasgow, :Inverness]
    :random.seed(1,2,3)
    assert TspSolver.Mutator.swap(individual) == expected
  end

  test "it swaps the elements 2 and 5 when seed is {11,22,33}" do
    individual = [:Aberdeen, :Dundee, :Perth, :Edinburgh, :Glasgow, :Inverness]
    expected   = [:Aberdeen, :Dundee, :Inverness, :Edinburgh, :Glasgow, :Perth]
    :random.seed(11,22,33)
    assert TspSolver.Mutator.swap(individual) == expected
  end
end
