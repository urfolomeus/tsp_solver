defmodule TspSolver.Helpers do

  @moduledoc"""
  A collection of helper methods that are used through out the app.
  """

  @doc"""
  Seeds Erlang's :random tasks to be as random as possible
  """
  def seed_random do
    << a::32, b::32, c::32 >> = :crypto.rand_bytes(12)
    :random.seed(a,b,c)
  end

  @doc"""
  Provides a random sample of n elememts from the given collection

  ## Example

  iex> :random.seed(1,2,3)
  iex> TspSolver.Helpers.sample(["a", "b", "c"], 2)
  ["b", "c"]
  """
  def sample(collection, n) do
    collection |> Enum.shuffle |> Enum.take(n)
  end

  @doc"""
  Returns the index of the given element within the given collection

  ## Example

  iex> index = TspSolver.Helpers.index_of(["a", "b", "c"], "b")
  1
  """
  def index_of(collection, element) do
    Enum.find_index collection, &(&1 == element)
  end
end
