defmodule TspSolver.Generator do
  import TspSolver.Helpers

  @cities [:Aberdeen, :Dundee, :Perth, :Edinburgh, :Glasgow, :Inverness]

  @moduledoc"""
  Generates the initial population for the genetic algorithm. In this instance
  it randomly generates n lists of cities for the TSP.
  """

  @doc"""
  Getter for city list.

  ## Example

  iex> TspSolver.Generator.cities()
  [:Aberdeen, :Dundee, :Perth, :Edinburgh, :Glasgow, :Inverness]
  """
  def cities(), do: @cities

  @doc"""
  Generates a new population of n lists of cities. Each city list is randomly
  ordered.
  """
  def generate_population(n) do
    seed_random
    _generate_population(n)
  end

  defp _generate_population(0), do: []
  defp _generate_population(n) do
    [Enum.shuffle(@cities) | _generate_population(n-1)]
  end
end
