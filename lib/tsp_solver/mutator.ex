defmodule TspSolver.Mutator do
  import TspSolver.Helpers


  @moduledoc"""
  Handles the mutation aspects of the genetic algorithm.

  One of the most powerful features of a genetic algorithm is its ability
  to randomly mutate indivduals within a population into other individuals.
  This mimics genetic mutation or aberation within nature.

  In practice it helps to prevent the algorithm from zeroing in on local optima
  when better solutions exist far from the current location in the search space.
  """

  @doc"""
  This mutation function uses the swap pattern. In the swap pattern
  two elements are selected at random from the list and their positions
  are swapped.
  """
  def swap(individual) do
    [first, last] = individual |> Enum.shuffle |> Enum.take(2)
    Enum.map(individual, fn (elem) -> cond do
      elem == first ->
        last
      elem == last ->
        first
      true ->
        elem
      end
    end)
  end
end
