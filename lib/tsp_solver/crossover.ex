defmodule TspSolver.Crossover do
  import TspSolver.Helpers

  @moduledoc"""
  The Crossover module handles the crossover algorithms for the genetic algorithm.
  Crossover is the method used to create a new generation of solutions from a given
  set of parent solutions and mimic breeding in nature. The resulting child solutions
  will have attributes of both parents.
  """

  @doc"""
  Permutation-style crossover. In the case where not all solutions are valid,
  this method of crossover ensures that no duplicate elements occur within a
  solution. This is achieved by splitting the parent solutions at the same split
  point, keeping the head intact and sorting the tail's elements to match the
  order they occur within the other child.
  """
  def permutation(parents, split_point) do
    [child_1, child_2] = parents

    {child_1_head, child_1_tail} = Enum.split(child_1, split_point)
    {child_2_head, child_2_tail} = Enum.split(child_2, split_point)

    [
      List.flatten([child_1_head | crossover_tails(child_1_tail, child_2)]),
      List.flatten([child_2_head | crossover_tails(child_2_tail, child_1)]),
    ]
  end

  # Sorts the given tail's elements into the order that they appear in the
  # other_child
  defp crossover_tails(tail, other_child) do
    Enum.sort tail, &( index_of(other_child, &1) < index_of(other_child, &2) )
  end
end
